using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsule : MonoBehaviour
{
    CharacterController character;

    float directionX, directionZ, reverseActivator;

    [SerializeField]
    float speed;

    [SerializeField]
    float jumpHeight;

    bool jump, blackBallActivator;

    Vector3 move;

    Material material;

    float initialY;

    // Start is called before the first frame update
    void Start()
    {
        initialY = transform.position.y;
        character = GetComponent<CharacterController>();
        material = GetComponent<Renderer>().material;
        reverseActivator = 1;
        jumpHeight = 2;
        speed = 5;
        jump = false;
        blackBallActivator = true;
    }

    // Update is called once per frame
    void Update()
    {
        directionX = reverseActivator * Input.GetAxis("Horizontal");
        directionZ = reverseActivator * Input.GetAxis("Vertical");

        if (character.isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        else if (transform.position.y >= initialY + jumpHeight)
        {
            jump = false;
        }

        //jump = character.isGrounded && Input.GetKeyDown(KeyCode.Space) && blackBallActivator;
    }

    private void FixedUpdate()
    {
        move.y = jump ? 9.81f * Time.fixedDeltaTime : -9.81f * Time.fixedDeltaTime;
        move.x = speed * directionX * Time.fixedDeltaTime;
        move.z = speed * directionZ * Time.fixedDeltaTime;

        character.Move(move);
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Red":
                material.color = Color.red;
                speed -= 2;
                break;
            case "Blue":
                material.color = Color.blue;
                jumpHeight += 2;
                break;
            case "Green":
                material.color = Color.green;
                speed += 2;
                break;
            case "Black":
                material.color = Color.black;
                blackBallActivator = false;
                break;
            case "Yellow":
                material.color = Color.yellow;
                reverseActivator = -1;
                break;
        }

        other.gameObject.SetActive(false);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "White")
        {
            hit.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-100,100), 0, Random.Range(-100,100)));
        }
    }
}

